import { TestBed } from '@angular/core/testing';

import { HotelInventoryFormResourcesService } from './hotel.service';

describe('HotelService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HotelInventoryFormResourcesService = TestBed.get(HotelInventoryFormResourcesService);
    expect(service).toBeTruthy();
  });
});
