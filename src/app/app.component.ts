import { Component, ViewChild, OnInit, EventEmitter, Output, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Subscription } from "rxjs";
import { HotelInventoryFormResourcesService } from '../app/hotel.service';
import Swal from 'sweetalert2'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy{
  title = 'hotetLandingpage';
  value;
  signInForm?: FormGroup;
  @Output() submitForm: EventEmitter<any> = new EventEmitter();
  submitted = false;
  subscriptions = new Subscription();
  constructor(
    private hotelInventoryFormResourcesService: HotelInventoryFormResourcesService,
    private formBuilder: FormBuilder
  ) {
    this.createForm();
  }

  doSendForm() {
    this.submitted = true;
    if (this.signInForm.invalid) {
      return;
    }
    this.hotelInventoryFormResourcesService.defaultHeaders = this.hotelInventoryFormResourcesService.defaultHeaders.set(
      'x-hotel-client-ip',
      '182.101.100.12'
    );
    this.hotelInventoryFormResourcesService.defaultHeaders = this.hotelInventoryFormResourcesService.defaultHeaders.set(
      'x-gtd-agency',
      'GOTADI'
    );
    this.hotelInventoryFormResourcesService.createHotelInventoryFormUsingPOST({
      address: this.signInForm.get('address').value,
      email: this.signInForm.get('email').value,
      fullName: this.signInForm.get('fullName').value,
      hotelName: this.signInForm.get('hotelName').value,
      phoneNumber: this.signInForm.get('phoneNumber').value
    }).subscribe(result => {
      if (result) {
        Swal.fire(
          'Thông báo',
          'Cảm ơn bạn đã gửi thông tin liên hệ, chúng tôi sẽ liên lạc với bạn trong thời gian sớm nhất.',
          'success',
        )
        this.submitted = false;
        this.createForm();
        return result;
      }
    },
      (error) => {
        Swal.fire(
          'Thông báo',
          'Đã có lỗi xảy ra vui lòng thử lại sau !',
          'error'
        )
        console.log(error);
      }
    );
  }


  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  createForm() {
    this.signInForm = this.formBuilder.group({
      address: ['', [Validators.required, Validators.maxLength(150)]],
      email: ['', [Validators.required, Validators.email, Validators.maxLength(100)]],
      fullName: ['', [Validators.required, Validators.maxLength(120)]],
      hotelName: ['', [Validators.required,Validators.maxLength(120)]],
      phoneNumber: ['', [Validators.required, Validators.pattern('^[0-9]+$'), Validators.maxLength(15), Validators.minLength(10)]]
    });
  }
}
